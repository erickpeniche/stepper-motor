#ifndef GLOBALDEFINITIONS_H
#define	GLOBALDEFINITIONS_H

#define true  1
#define false 0

#define _ON  1
#define _OFF 0

#define _INPUT 1
#define _OUTPUT 0

#define P1A PORTCbits.RC2
#define P1B PORTDbits.RD5
#define P1C PORTDbits.RD6
#define P1D PORTDbits.RD7

#define LEDL PORTBbits.RB0
#define LEDR PORTBbits.RB1

#endif	/* GLOBALDEFINITIONS_H */