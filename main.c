#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include "Config.h"
#include "lcd.h"

/**
* PIC Configurations
*/
#pragma config FOSC = XT
#pragma config WDTE = OFF
#pragma config PWRTE = OFF
#pragma config MCLRE = ON
#pragma config CP = OFF
#pragma config CPD = OFF
#pragma config BOREN = OFF
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF

#pragma config BOR4V = BOR40V
#pragma config WRT = OFF

void interrupt isr(void);
void run_motor();
void set_timer2(int prsc);
void stop_motor(void);
void start_lcd(void);
void read_adc(void);
float convert_to_rpm(int adc);
float convert_to_dc(int adc);

float pwmfreq = 4500.0;
float pwmperiod = 0.0;
float oscfreq = 4000000.0;
float oscperiod = 0.0;
float dcadc = 0.0;
int pr2value = 0;
int maxdutycycle = 0;
int dutycycle = 0;
int started = 0;
int motordirection = 1;

unsigned char buffer1[20];
unsigned char buffer2[20];

int adc = 0;
float rpm = 0;

int main(int argc, char** argv) {
	config_board();
	start_lcd();

	while(1) {
		read_adc();
		if(PORTDbits.RD0 == 1) {
			motordirection = 1;
			run_motor();
			LEDL = _ON;
		}
		else if(PORTDbits.RD1 == 1) {
			motordirection = 0;
			run_motor();
			LEDR = _ON;
		}
		else {
			PORTB = _OFF;			// Turn off leds
			stop_motor();
		}
	}

	return (EXIT_SUCCESS);
}

void interrupt isr(void) {
	if (PIR1bits.TMR2IF == 1 && started == 0) {				// Se activa el evento especial
		started = 1;
		if(motordirection == 1) {
			TRISCbits.TRISC2 = _OUTPUT;						// CCP1 => OUTPUT
			TRISDbits.TRISD7 = _OUTPUT;						// CCP1 => OUTPUT
			TRISDbits.TRISD6= _INPUT;						// CCP1 => OUTPUT
			TRISDbits.TRISD5 = _INPUT;						// CCP1 => OUTPUT

			P1A = P1D = _ON;
			P1B = P1C = _OFF;
		}
		else {
			TRISCbits.TRISC2 = _INPUT;						// CCP1 => OUTPUT
			TRISDbits.TRISD7 = _INPUT;						// CCP1 => OUTPUT
			TRISDbits.TRISD6= _OUTPUT;						// CCP1 => OUTPUT
			TRISDbits.TRISD5 = _OUTPUT;						// CCP1 => OUTPUT

			P1A = P1D = _OFF;
			P1B = P1C = _ON;
		}
	}
	PIR1bits.TMR2IF = 0;
}

void read_adc(void) {
	ADCON0bits.GO = 1;

	while (ADCON0bits.nDONE) {
		adc = ADRESH;
		adc = adc<<8;
		adc = adc+ADRESL;
		dcadc = convert_to_dc(adc);
		//rpm = convert_to_rpm(adc);
	}

	sprintf(buffer1, "DC: %1.2f %%", dcadc);									//convertimos el valor en ASCII
	Lcd_Out2(1, 1, buffer1);

	// sprintf(buffer, "RPM: %1.2f",rpm);									//convertimos el valor en ASCII
	// Lcd_Out2(1, 1, buffer);
}

void stop_motor(void) {
	TRISCbits.TRISC2 = _INPUT;						// CCP1 => OUTPUT
	TRISDbits.TRISD7 = _INPUT;						// CCP1 => OUTPUT
	TRISDbits.TRISD6= _INPUT;						// CCP1 => OUTPUT
	TRISDbits.TRISD5 = _INPUT;						// CCP1 => OUTPUT

	PIE1bits.TMR2IE = 0;														// Enables the TMR2 interrupt
	PIR1bits.TMR2IF = 0;														// Clear TMR2 Flag
	P1A = P1B = P1C = P1D = _OFF;
	started = 0;
}

void run_motor() {
	dutycycle = (int)dcadc;
	oscperiod = (float)(1.0/oscfreq);
	pwmperiod = (float)(1.0/pwmfreq);

	for (int i = 1; i <= 16; i = i*4)
	{
		pr2value = (int)(pwmperiod/(i*oscperiod*4.0)-1.0);
		if(pr2value > 255 || pr2value < 1) {
			continue;
		}
		else {
			maxdutycycle = (int)(pwmperiod/(oscperiod*i));
			dutycycle = (int)((dcadc/100.0)*((float)maxdutycycle));

			PIE1bits.TMR2IE = 1;														// Enables the TMR2 interrupt
			PIR1bits.TMR2IF = 0;														// Clear TMR2 Flag

			// Configure CCP1
			PR2 = (int)pr2value;
			if(motordirection == 1) {
				CCP1CON = 64 + ((dutycycle & 3) << 4) + 12;								// Motor runs forward
			}
			else {
				CCP1CON = 192 + ((dutycycle & 3) << 4) + 12;								// Motor runs reverse
			}
			CCPR1L = dutycycle >> 2;

			// Configure TMR2
			set_timer2(i);
			break;
		}
	}
}

void set_timer2(int prsc) {
	if(prsc == 1) {
		T2CON = 0b00000100;
	}
	else if(prsc == 4) {
		T2CON = 0b00000101;
	}
	else if(prsc == 16) {
		T2CON = 0b00000111;
	}
}

void start_lcd(void) {
	Lcd_Init();
	Lcd_Cmd(LCD_CLEAR);
	Lcd_Cmd(LCD_CURSOR_OFF);
	__delay_ms(100);
}

float convert_to_rpm(int adc) {
	return 1.0;
}

float convert_to_dc(int adc) {
	return (float)(((float)adc*100.0)/1023.0);
}